<?php

namespace App\Http\Controllers;

use App\Exports\UsersExport;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade as PDF;

class UsuariosController extends Controller {

    public function index() {   
        return view('panel.index');
    }

    public function get_users() {
        $users = User::select('id', 'name', 'email', 'enabled')
                        ->where('id', '!=', auth()->user()->id)
                        ->get();
        
        return Datatables()->of($users)->toJson();
    }

    public function get_user(Request $request) {

        $user = User::select('id', 'name', 'password', 'email', 'enabled')
                        ->where('id', '=', $request->user_id)
                        ->get();
        return $user;
    }

    public function add_user(Request $request) {   
        return User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'enabled' => $request->get('enabled')
        ]);
    }

    public function update_user(Request $request) {
        
        $user = User::find($request->get('id'));

        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->enabled = $request->get('enabled');

        $flag = $user->update();

        return response()->json(['success' => $flag]);
    }

    public function update_enabled(Request $request) {
        
        $user = User::find($request->user_id);
        $user->enabled = $request->enabled;

        $flag = $user->update();

        return response()->json(['success' => $flag]);
    }

    public function update_password(Request $request) {
        
        $user = User::find($request->id);
        $user->password = Hash::make($request->password);

        $flag = $user->update();

        return response()->json(['success' => $flag]);
    }

    public function exportExcel() {
        //return User::select('id', 'name', 'email', 'enabled')->get();
        return Excel::download(new UsersExport, 'usuarios.xlsx');
    }

    public function exportPDF() {
        $users = User::select('id', 'name', 'email', 'enabled')->get();
        $pdf = PDF::loadView('panel.vistaPDF', compact('users'));
        return $pdf->stream('usuarios.pdf');
    }

}
