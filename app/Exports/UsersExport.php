<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection, WithHeadings
{
    public function headings(): array {
        return ['Id', 'Nombre de Usuario', 'Correo Electronico'];
    }

    public function collection()
    {
        $user = User::select('id', 'name', 'email')->get();
        return $user;
    }
}
