<?php

use App\Http\Controllers\UsuariosController;
use Illuminate\Support\Facades\Route;

Route::get('inicio', [UsuariosController::class, 'index'])->name('panel.inicio');
Route::get('get_users', [UsuariosController::class, 'get_users'])->name('get_users');
Route::post('get_user', [UsuariosController::class, 'get_user'])->name('get_user');
Route::post('add_user', [UsuariosController::class, 'add_user'])->name('add_user');
Route::post('update_user', [UsuariosController::class, 'update_user'])->name('update_user');
Route::post('update_enabled', [UsuariosController::class, 'update_enabled'])->name('update_enabled');
Route::post('update_password', [UsuariosController::class, 'update_password'])->name('update_password');

Route::get('export_excel' , [UsuariosController::class, 'exportExcel'])->name('export_excel');
Route::get('export_pdf' , [UsuariosController::class, 'exportPDF'])->name('export_pdf');