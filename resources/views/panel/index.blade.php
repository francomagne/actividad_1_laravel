@extends('adminlte::page')

@section('title', 'Inicio')

@section('content_header')
    
@stop

@section('content')

<div class="container">

    <h1 class="text-center">Lista de Usuarios</h1>
    
    @if (Auth::user()->enabled == 1)
    
        <a id="nuevo_usuario" data-toggle="modal" data-target="#modal-nuevo-usuario" role="button" href="#" class="btn btn-primary mb-4"> <i class="fas fa-plus"></i> NUEVO USUARIO </a>

        <div style="float: right;">
            <a id="pdf" href="{{ route('export_pdf') }}" target="_blank" class="btn bg-red mb-4" title="Descargar PDF"> <i class="far fa-file-pdf"></i> </a>
            <a id="excel" href="{{ route('export_excel') }}" class="btn bg-green mb-4" title="Descargar EXCEL"> <i class="far fa-file-excel"></i> </a>
        </div>

        <div id="modal-nuevo-usuario" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" data-update="0" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-backdrop="static">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">

                    <div class="modal-header">
                        <h6 class="modal-title"></h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <form id="form-nuevo-usuario" class="needs-validation" novalidate>

                        <div class="modal-body">

                            <div class="container">

                                <div class="row">

                                    <div class="col-12 col-md-12 col-sm-12">

                                        <div class="form-row">
                                            
                                            @csrf

                                            <input id="id" type="hidden" name="id" value="0">

                                            <div class="form-group col-md-6">
                                                <label class="font-weight-bold" for="name">Nombre de Usuario <span class="text-danger">*</span> </label>
                                                <input name="name" type="text" class="form-control" required>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="font-weight-bold" for="email">Correo Electrónico <span class="text-danger">*</span></label>
                                                <input name="email" type="text" class="form-control" required>
                                            </div>

                                        </div>

                                        <div class="form-row">

                                            <div class="form-group col-md-6">
                                                <label class="font-weight-bold" for="password">Contraseña <span class="text-danger">*</span></label>
                                                <input name="password" type="password" class="form-control" required>
                                            </div>

                                            <div class="form-group col-md-6">

                                                <label class="font-weight-bold" for="enabled"> Estado <span class="text-danger">*</span></label>

                                                <select id="estado-usuario" name="enabled" class="form-control" required>
                                                    <option value="1">Habilitado</option>
                                                    <option value="0">Deshabilitado</option>
                                                </select>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary"></button>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <table id="tabla-usuarios" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%;">
            <thead class="bg-primary text-white">
                <tr align="center">
                    <th scope="col"> ID </th>
                    <th scope="col"> Nombre </th>
                    <th scope="col"> Correo </th>
                    <th scope="col"> Estado </th>
                    <th scope="col"> Acciones </th>
                </tr>
            </thead>
        </table>

    @else
    <div class="alert alert-danger" role="alert">
        Su usuario se encuentra deshabilitado!
    </div>
    @endif
</div>
@stop

@section('css')

@stop

@section('js')
    <script src="{{ asset('js/usuarios.js') }}"></script>
@stop
