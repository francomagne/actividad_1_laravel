<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
 
    <title>Lista de Usuarios</title>
</head>
<body> 

    <div class="container-fluid">
        
        <h3 class="text-center mt-4"> Lista de Usuarios </h3>
    
        <table class="table table-striped table-bordered shadow-lg">
            <thead>
                <tr class="text-center"> 
                    <th scope="col">ID</th>
                    <th scope="col">Nombre de Usuario</th>
                    <th scope="col">Correo Electronico</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                <tr class="text-center"> 
                    <td>{{$user->id}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                </tr>
                @endforeach

            </tbody>
        </table>

    </div>
</body>
</html>