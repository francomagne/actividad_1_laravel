let jq = jQuery.noConflict();

jq(document).ready(function() {

    const ESTADO_DANGER = 0;
    const ESTADO_SUCCESS = 1;
    
    jq('#tabla-usuarios').DataTable({
        responsive: true,
        ajax: "get_users",
        fnCreatedRow: function (row, data, iDataIndex) {
            jq(row).attr('id', data['id']);
        },
        columns: [
            { 
                data: "id",
                render: function(data, type, row) {
                    return createRowCellCenter(data);
                }
            },
            { 
                data: "name",
                render: function(data, type, row) {
                    return createRowCellCenter(data);
                }
            },
            { 
                data: "email",
                render: function(data, type, row) {
                    return createRowCellCenter(data);
                }
            },
            {
                data: "enabled",
                render: function(data, type, row) {
                    td = "";

                    if(data == 1) {
                        td += createTableEstadoRow('Habilitado', ESTADO_SUCCESS);
                    } else {
                        td += createTableEstadoRow('Deshabilitado', ESTADO_DANGER);
                    }

                    return td;
                }
            },
            {
                defaultContent: '',
                orderable: false,
                render: function(data, type, row) {
                    return createRowCellCenter(createButtonOptions(row.id, row.enabled));
                }
            }
        ],
        "lengthMenu": [[5,10,20,-1],[5,10,20,'All']],
        "language": {
            "sProcessing": "Procesando...",
            "sLoadingRecords": "Cargando...",
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "Sin usuarios cargados",
            "info": "Mostrando la pagina _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar:",
            "paginate":{
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
    });

    jq(document).on('click', '#nuevo_usuario', function() {

        jq('#modal-nuevo-usuario .modal-title').text('Registrar Nuevo Usuario');

        jq('#modal-nuevo-usuario .modal-footer button[type=submit]').text('Registrar Usuario');

        jq('#modal-nuevo-usuario').attr('data-update', 0);

    });

    jq(document).on('click', '.editar', function() {

        var user_id = jq(this).val();

        jq('#modal-nuevo-usuario .modal-title').text('Actualizar Usuario');

        jq('#modal-nuevo-usuario .modal-footer button[type=submit]').text('Actualizar Usuario');

        jq('#modal-nuevo-usuario').attr('data-update', 1);

        let data_update = jq('#modal-nuevo-usuario').attr('data-update');

        jq.ajax({
            type: 'POST',
            url: 'get_user',
            data: { 
                user_id: user_id,
                _token: jq("input[name=_token]").val()
            },
            dataType: "json",
            success: function(json) {
                parseInfoUser(json[0], data_update);
            }
        })
        .done(function( json ) {
            //console.log('Done', json);
        })
        .fail(function (jqXHR, textStatus, errorThrown){
            console.log("Error", textStatus, errorThrown);
        });
    });

    
    jq(document).on('click', '.habilitar', function() {
        
        let user_id = jq(this).val();
        let mensaje = '¿Estas seguro de Habilitar al usuario con id '+ user_id +'?'
        let title = 'Habilitar Usuario';
        let enabled = 1;

        createModalModificarEstado(user_id, title, mensaje, enabled);
    });

    jq(document).on('click', '.deshabilitar', function() {
        
        let user_id = jq(this).val();
        let mensaje = '¿Estas seguro de Deshabilitar al usuario con id '+ user_id +'?'
        let title = 'Deshabilitar Usuario';
        let enabled = 0;

        createModalModificarEstado(user_id, title, mensaje, enabled);

    });

    jq(document).on('click', '.password', function() {

        let user_id = jq(this).val();
        let title = 'Cambiar Contraseña';

        jq.ajax({
            type: 'POST',
            url: 'get_user',
            data: {
                user_id: user_id,
                _token: jq("input[name=_token]").val()
            },
            dataType: "json",
            success: function(json) {
                createModalModificarPassword(user_id, title, json[0]);
            }
        })
        .done(function( json ) {
            //console.log('Done', json);
        })
        .fail(function (jqXHR, textStatus, errorThrown){
            console.log("Error", textStatus, errorThrown);
        });

    });

    jq(document).on('submit', '#form-nuevo-usuario', function(e){
        e.preventDefault();

        let form = 'form-nuevo-usuario';

        let data_update = jq('#modal-nuevo-usuario').attr('data-update');

        if(validarFormUser(form, data_update)) {

            let url = '';

            if(data_update == 0) {
                url = 'add_user';
            } else {
                url = 'update_user';
            }

            let data = jq('#' + form).serialize();

            //console.log(data);

            jq.ajax({
                type: 'POST',
                url: url,
                data: data,
                dataType: "json",
                success: function(json) {
                    //console.log(json);
                    jq('#tabla-usuarios').DataTable().ajax.reload();
                    jq('#modal-nuevo-usuario').modal('hide');
                }
            })
            .done(function( json ) {
                //console.log('Done', json);
            })
            .fail(function (jqXHR, textStatus, errorThrown){
                console.log("Error", textStatus, errorThrown);
            });
        }        
    });

    jq(document).on('click', '#guardar-estado', function() {

        let user_id = jq('#modal-modificar-estado').attr('data-id');
        let enabled = jq('#modal-modificar-estado').attr('data-enabled');

        jq.ajax({
            type: 'POST',
            url: 'update_enabled',
            data: {
                user_id: user_id,
                enabled: enabled,
                _token: jq("input[name=_token]").val()
            },
            dataType: "json",
            success: function(json) {
                //console.log(json);
                jq('#tabla-usuarios').DataTable().ajax.reload();
                jq('#modal-modificar-estado').modal('hide');
            }
        })
        .done(function( json ) {
            //console.log('Done', json);
        })
        .fail(function (jqXHR, textStatus, errorThrown){
            console.log("Error", textStatus, errorThrown);
        });

    });

    jq(document).on('submit', '#form-resetear-password', function(e) {
        e.preventDefault();

        let password = jq('#form-resetear-password input[name="password_usuario"]');

        if(!isEmpty(password)) {
            removerInvalido(password);

            let data = jq(this).serializeArray();

            jq.ajax({
                type: 'POST',
                url: 'update_password',
                data: {
                    id: data[0].value,
                    password: data[1].value,
                    _token: jq("input[name=_token]").val()
                },
                dataType: "json",
                success: function(json) {
                    //console.log(json);
                    jq('#tabla-usuarios').DataTable().ajax.reload();
                    jq('#modal-modificar-password').modal('hide');
                }
            })
            .done(function( json ) {
                //console.log('Done', json);
            })
            .fail(function (jqXHR, textStatus, errorThrown){
                console.log("Error", textStatus, errorThrown);
            });

        } else {
            campoInvalido(password, 'Campo Requerido');
        }

    });

    jq(document).on('hidden.bs.modal','#modal-nuevo-usuario', function() {
        jq('#id').val(0);
        removerInvalido(jq("#form-nuevo-usuario input[name='name']"));
        removerInvalido(jq("#form-nuevo-usuario input[name='password']"));
        removerInvalido(jq("#form-nuevo-usuario input[name='email']"));
        removerInvalido(jq("#form-nuevo-usuario input[name='enabled']"));
        jq(this).find('#form-nuevo-usuario')[0].reset();
        jq("#form-nuevo-usuario input[name='password']").prop('disabled', false);
    });

    jq(document).on('hidden.bs.modal','#modal-modificar-estado', function() {
        jq(this).remove();
    });

    jq(document).on('hidden.bs.modal','#modal-modificar-password', function() {
        jq(this).remove();
    });

    /******************  Funciones Auxiliares *********************/
    
    function validarFormUser(form, update){

        let isValid = true;

        let name = jq("#" + form + " input[name='name']");
        let email = jq("#" + form + " input[name='email']");
        let password = jq("#" + form + " input[name='password']");
        let enabled = jq("#" + form + " select[name='enabled']");

        if(isEmpty(name)){
            isValid = false;
            campoInvalido(name, 'Campo Requerido');
        }else{
            removerInvalido(name);
        }

        if(isEmpty(email)){
            isValid = false;
            campoInvalido(email, 'Campo Requerido');
        }else{
            removerInvalido(email);
        }

        if(update == 0) {
            if(isEmpty(password)){
                isValid = false;
                campoInvalido(password, 'Campo Requerido');
            }else{
                removerInvalido(password);
            }
        }

        if(isEmpty(enabled)){
            isValid = false;
            campoInvalido(enabled, 'Campo Requerido');
        }else{
            removerInvalido(enabled);
        }

        return isValid;
    }

    function parseInfoUser(user, update) {

        let form = 'form-nuevo-usuario';

        let id = jq('#id');
        id.val(user['id']);

        let name = jq("#" + form + " input[name='name']");
        name.val(user['name']);

        let email = jq("#" + form + " input[name='email']");
        email.val(user['email']);

        let password = jq("#" + form + " input[name='password']");
        if(update == 1) {
            password.prop('disabled', true);
        }
        password.val(user['password']);

        let enabled = jq("#" + form + " select[name='enabled']");
        enabled.val(user['enabled']);
    }

    function isEmpty(field){
        if(jq.trim(field.val()) == ''){
          return true;
        }else{
          return false;
        }
    }
    
    function addInvalidFeedback(message){
        let div = '<div class="invalid-feedback">';
            div += message;
        div += '</div>';
        return div;
    } 

    function campoInvalido(field, message){
        field.addClass('is-invalid');                  
        if(!field.parent().children().last().hasClass('invalid-feedback')) {
            field.parent().append(addInvalidFeedback(message));  
        }  
        field.parent().find('.invalid-feedback').text(message);
    }
  
    function removerInvalido(field){
        field.removeClass('is-invalid');     
        field.parent().find('invalid-feedback').remove();
    }

    function createButtonOptions(id, enabled) {

        let options = '<button data-toggle="modal" data-target="#modal-nuevo-usuario" class="btn btn-primary editar mr-1" value='+id+' title="Editar"><i class="fas fa-edit"></i></button>';

        options += '<button role="button" href="#" class="btn btn-warning password mr-1" value="'+id+'" title="Contraseña"><i class="fas fa-key"></i></button>';

        if(enabled == 1) {
            options += '<button role="button" href="#" class="btn btn-danger deshabilitar" value="'+id+'" title="Deshabilitar"><i class="fas fa-times"></i></button>';
        } else {
            options += '<button role="button" href="#" class="btn btn-success habilitar" value="'+id+'" title="Habilitar"><i class="fas fa-check"></i></button>';
        }

        return options;
    }

    function getEstadoBadge(value, type){
	
        let badge;
    
        switch(type){
            case ESTADO_DANGER:
                badge = '<h5><span class="badge badge-danger">'+ value +'</span></h5>';
                break;
            case ESTADO_SUCCESS:
                badge = '<h5><span class="badge badge-success">'+ value +'</span></h5>';
                break;
            default:
                badge = '<h5>Estado indefinido</h5>';
        }
    
        return badge;
    }

    function createTableEstadoRow(value, type){
        let div = '<div class="text-center">';
                div += getEstadoBadge(value, type);
            div += '</div>';
        return div;
    }

    function createRowCellCenter(value){
        let div = '<div class="text-center">';
                div += value;
            div += '</div>';
        return div;
    }

    function createModalModificarEstado(id, title, mensaje, enabled) {
        let modal = '<div class="modal fade" id="modal-modificar-estado" tabindex="-1" data-id="'+id+'" data-enabled="'+enabled+'"role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">';
        modal += '<div class="modal-dialog modal-dialog-centered" role="document">';
            modal += '<div class="modal-content">';

                modal += createHeaderModal(title);

                modal += '<div class="modal-body">';
                modal += '<p>';
                modal += mensaje;
                modal += '</p>';
                modal += '</div>';

                modal += '<div class="modal-footer">';
                    modal += '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>';
                    modal += '<button id="guardar-estado" type="button" class="btn btn-primary">Aceptar</button>';
                modal += '</div>';

            modal += '</div>';
        modal += '</div>';
        modal += '</div>';
        jq('#tabla-usuarios').after(modal);
        jq('#modal-modificar-estado').modal('show');
    }

    function createModalModificarPassword(id, title, user){

        let modal = '<div class="modal fade" id="modal-modificar-password" tabindex="-1" data-id="'+id+'" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">';
        modal += '<div class="modal-dialog modal-dialog-centered" role="document">';
            modal += '<div class="modal-content">';

                modal += createHeaderModal(title);

                modal += '<form id="form-resetear-password" class="needs-validation" novalidate>';
 
                modal += '<div class="modal-body">';

                    modal += '<div class="container-fluid">';

                    modal += '<div class="row">';

                        modal += createBoxTitleModal("Nombre de Usuario", user['name']);

                        modal += createBoxTitleModal("Correo Electronico", user['email']);

                    modal += '</div>';

                    modal += '</div>';

                    modal += createInputHidden('id', user['id']);

                    modal += '<div class="form-group col-md-12">';
                    modal += '<h6 class="font-weight-bold" for="password_usuario">Nueva Contraseña</h6>';
                    modal += '<input type="text" class="form-control" name="password_usuario" aria-describedby="inputGroupPrepend2" placeholder="Escriba una contraseña">'
                    modal += '</div>';                
            
                modal += '</div>';

                modal += createFooterModal('Cancelar', 'Resetear');

                modal += '</form>';;

            modal += '</div>';
        modal += '</div>';
        modal += '</div>';
        jq('#tabla-usuarios').after(modal);
        jq('#modal-modificar-password').modal('show');
    }

    function createInputHidden(name, value){
        let div = '<div class="form-group col-sm-12 col-md-12 col-lg-12 col-xl-12">';
            div += '<input type="hidden" class="form-control" value='+ value +' name="' + name + '" aria-describedby="inputGroupPrepend2" readOnly>';
        div += '</div>';   
        return div;
    }

    function createHeaderModal(title){
        let header = '<div class="modal-header bg-light">';
            header += '<h6 class="modal-title" id="exampleModalLabel">' + title + '</h6>';
            header += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
                header += '<span aria-hidden="true">&times;</span>';
            header += '</button>';
        header += '</div>';
        return header;
    }

    function createFooterModal(cancel, confirm){
        let footer = '<div class="modal-footer">';

            footer += '<button class="btn btn-secondary" data-dismiss="modal">' + cancel +'</button>';
            footer += '<button id="btn-cambiar-estado" type="submit" type="button" class="btn btn-primary"> '+ confirm +'</button>';

        footer += '</div>';
        return footer;
    }

    function createBoxTitleModal(title, text){
        let div = '<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">';
                div += '<h6 class="font-weight-bold">';
                div += title;
                div += '</h6>';
                div += '<p>';
                div += text;
                div += '</p>';
        div += '</div>';
        return div;
    }

});